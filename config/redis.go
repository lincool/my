package config

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
)

var (
	Ctx = context.Background()
	Rdb *redis.Client
)

func Redis() {
	client := redis.NewClient(&redis.Options{
		Addr:     "192.168.0.50:6379",
		Password: "123456789", // no password set
		DB:       1,           // use default DB
	})
	pong, err := client.Ping(context.Background()).Result()
	if err != nil {
		fmt.Println(err)

		//"redis connect ping failed, err:", zap.Error(err)
	} else {
		fmt.Println("pong", pong)
		//Log.Info("redis connect ping response:", zap.String("pong", pong))
		Rdb = client

	}
}

//序列化
func Serializer(value interface{}) (result []byte, err error) {
	result, err = json.Marshal(value)
	return result, err
}

// deserializer 反序列化 将字节流转换为go结构体对象
func Deserializer(value []byte, result interface{}) (interface{}, error) {
	err := json.Unmarshal(value, &result)
	if err != nil {
		//log.Log().Error("get data failed, err:%v\n", err)
		return nil, err
	}
	return result, nil
}
