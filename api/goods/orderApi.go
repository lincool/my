package goods

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)
type OrderApi struct {}
func GetorderList(c *fiber.Ctx) error {
	userId := c.Params("userId")

	userInt, err := strconv.ParseInt(userId, 10, 64)
	if err != nil {
		return err
	}
	orderList ,err:=  orderService.GetAll(userInt)
	return c.JSON(orderList)
}
