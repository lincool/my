package v1

import (
	"api-fiber-gorm/api/goods"
	"api-fiber-gorm/api/member"
)

type ApiGroup struct {
	MemberApiGroup   member.ApiGroup
	GoodsApiGroup  goods.ApiGroup
}

var ApiGroupApp = new(ApiGroup)
