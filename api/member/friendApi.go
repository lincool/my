package member

import (
	"api-fiber-gorm/common/response"
	"github.com/gofiber/fiber/v2"
	"strconv"
)

type FriendApi struct{}

func GetFriendList(c *fiber.Ctx) error {

	id := c.Query("id")
	uid, _ := strconv.Atoi(id)
	friendList, err := friendService.FindAllByMyId(uid)

	if err != nil {
		return err
	}
	return response.OkWithData(friendList, c)
	//return c.JSON(fiber.Map{"status": "success", "message": "Hello i'm ok!", "data": nil})

}
