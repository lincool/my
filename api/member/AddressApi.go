package member

import (
	"api-fiber-gorm/common/request"
	"api-fiber-gorm/common/response"
	"api-fiber-gorm/config"
	"context"
	"github.com/gofiber/fiber/v2"
	"strconv"
)

type AddressApi struct{}

var ctx = context.Background()

func GetAdd(c *fiber.Ctx) error {

	var userId string = c.Params("userId")
	userInt, err := strconv.ParseInt(userId, 10, 64)
	if err != nil {
		return err
	}
	userList, err := addressService.GetAddress(userInt)
	return c.JSON(userList)
	//return c.JSON(fiber.Map{"status": "success", "message": "Hello i'm ok!", "data": nil})

}
func GetAddressPage(c *fiber.Ctx) error {
	pageInfo := new(request.PageInfo)
	if err := c.QueryParser(pageInfo); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})

	}
	err := config.Rdb.Set(config.Ctx, "test_1", "value_1", 0).Err()
	if err != nil {

	}

	/*	if err := c.BodyParser(pageInfo); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})

	}*/
	/*	if err := utils.Verify(pageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return err
	}*/
	List, total, err := addressService.FindAll(pageInfo)
	if err != nil {
		return nil
	}
	//return	response.OkWithData(List,c)

	return response.OkWithDetailed(response.PageResult{
		List:     List,
		Total:    total,
		Page:     pageInfo.Page,
		PageSize: pageInfo.PageSize,
	}, "获取成功", c)
}
