package member

import "api-fiber-gorm/service"

type ApiGroup struct {
	AddressApi
}

var (
	addressService = service.ServiceGroupApp.MemberServiceGroup.AddressService
	friendService  = service.ServiceGroupApp.MemberServiceGroup.FriendService
)
