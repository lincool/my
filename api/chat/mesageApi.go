package chat

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)

type MessageApi struct {
}

func GetMessageListApi(c *fiber.Ctx) error {
	userId := c.Query("userId")
	uid, _ := strconv.Atoi(userId)
	list := messageService.GetMessageList(uid)
	return c.JSON(list)
}
