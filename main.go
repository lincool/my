package main

import (
	"api-fiber-gorm/config"
	"api-fiber-gorm/database"
	"api-fiber-gorm/router"
	"api-fiber-gorm/ws"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/websocket/v2"
	"log"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	app.Use(cors.New(cors.ConfigDefault))
	//app.Use(cors.New())
	database.ConnectDB()

	app.Use(func(c *fiber.Ctx) error {
		if websocket.IsWebSocketUpgrade(c) { // Returns true if the client requested upgrade to the WebSocket protocol
			return c.Next()
		}
		return c.Next() //c.SendStatus(fiber.StatusUpgradeRequired)
	})
	go ws.RunHub()
	//初始化
	config.Redis()
	router.SetupRoutes(app)
	log.Fatal(app.Listen(":3000"))
}
