package model

import "gorm.io/gorm"

type Friend struct {
	gorm.Model
	MyId   int
	UserId int
}
