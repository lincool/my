package model

import (
	"gorm.io/gorm"
)

type Order struct {
	gorm.Model
	GoodsId int
	//Goods goods
	Amount  int
	Number int
	UserId int
	//User user
}

type user struct {
	gorm.Model
	Username string `gorm:"unique_index;not null" json:"username"`
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Names    string `json:"names"`
}
type goods struct {
	gorm.Model
	GoodsName  string
	Price  uint32 `gorm:"default:0"`
	Status bool  `gorm:"default:false"`
}