package model

import "gorm.io/gorm"

type Message struct {
	gorm.Model
	UserId int
	SendId int
	// SendType uint 群暂时不考虑
	MsgType int
	IsRead  bool
	Data    string
}
