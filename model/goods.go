package model

import "gorm.io/gorm"

type Goods struct {
	gorm.Model
	GoodsName  string
	Price  uint32 `gorm:"default:0"`
	Status bool  `gorm:"default:false"`
}
