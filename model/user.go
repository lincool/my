package model

import "gorm.io/gorm"

// User struct
type User struct {
	gorm.Model
	Username string    `gorm:"unique_index;not null" json:"username"`
	Email    string    `gorm:"unique_index;not null" json:"email"`
	Password string    `gorm:"not null" json:"password"`
	Names    string    `json:"names"`
	Nickname string    `json:"nickname"`
	Headimg  string    `json:"headimg"`
	Address  []Address `gorm:"foreignKey:UserID"`
}

/*type AddressEntity struct {
	gorm.Model
	UserId uint
	AddressName string

}*/
