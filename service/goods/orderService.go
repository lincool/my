package goods

import (
	"api-fiber-gorm/database"
	"api-fiber-gorm/model"
)

type OrderService struct {

}

func (s OrderService) GetAll(userId int64)(orderList[] model.Order,err error)   {
	db := database.DB
	db.Where("UserId=?",userId).Find(&orderList)
	return orderList, err
}
