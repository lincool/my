package chat

import (
	"api-fiber-gorm/database"
	"api-fiber-gorm/model"
)

type MessageService struct {
}

func (messageService *MessageService) SaveMessage(message interface{}) {
	db := database.DB
	//db.Save(message)
	db.Create(message)

}
func (messageService *MessageService) GetMessageList(id int) (messageList []model.Message) {
	db := database.DB
	db.Where("UserId=?", id).Or("SendId=?", id).Order("IsRead DESC,ID DESC").Limit(10).Find(&messageList)
	return messageList
}

/*查询 最新的top 10 联系人+消息*/

/**
SELECT * FROM(
SELECT
	*
FROM
	`t_Message`
WHERE
	(UserId = 1 OR SendId = 1)
AND `t_Message`.`DeletedAt` IS NULL
ORDER BY
	IsRead DESC,
	ID DESC
LIMIT 9999
) a
GROUP BY  a.UserId+a.SendId
*/

func (messageService *MessageService) GetMessageListTop() {

}
