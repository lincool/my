package member

import (
	"api-fiber-gorm/database"
	"api-fiber-gorm/model"
)

type FriendService struct{}

type FinendList struct {
	model.Friend
	Nickname string `json:"nickname"`
	Headimg  string `json:"headimg"`
}

func (friendService *FriendService) FindAllByMyId(uid int) (friendList []FinendList, err error) {
	db := database.DB
	//var FriendList[] model.Friend
	var queryWhere model.Friend
	queryWhere.MyId = uid
	db.Table("t_friend as f").Select("*").Joins("left join t_user as u on u.ID= f.UserId").Find(&friendList, queryWhere)
	return friendList, err
}
