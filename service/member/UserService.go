package member

import (
	"api-fiber-gorm/database"
	"api-fiber-gorm/model"
	"fmt"
)

type UserService struct{}

func(userService *UserService) getUser(id uint)(user model.User)  {
	db := database.DB
	db.Find(&user, id)
	fmt.Println("user:",user)
	return  user
}