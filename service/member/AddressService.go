package member

import (
	"api-fiber-gorm/common/request"
	"api-fiber-gorm/database"
	"api-fiber-gorm/model"
)

type  AddressService struct {}
func (addressService *AddressService) GetAddress(userId int64)(addressList [] model.Address,err error)  {
	db := database.DB
	db.Limit(1).Offset(0).Where("UserId=?",userId).Find(&addressList)
	//db.Where("UserId=?",userId).Find(&addressList)
	return addressList,err
}
func (addressService *AddressService) FindAll(info *request.PageInfo) (addressList []*model.Address, total int64, err error) {
	db := database.DB
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)

	//db = db.Where("userId=?",uid )
	err = db.Model(model.Address{}). Count(&total).Error
	if err != nil {
		return addressList, total, err
	} else {
		err = db.Limit(limit).Offset(offset).Find(&addressList).Error
	}
	return addressList, total, err
}