package service

import (
	"api-fiber-gorm/service/chat"
	"api-fiber-gorm/service/goods"
	"api-fiber-gorm/service/member"
)

type ServiceGroup struct {
	MemberServiceGroup member.ServiceGroup
	GoodsServiceGroup  goods.ServiceGroup
	ChatServiceGroup   chat.ServiceGroup
}

var ServiceGroupApp = new(ServiceGroup)
