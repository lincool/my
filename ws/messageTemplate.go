package ws

import "time"

type MessageTemplate struct {
	UserId      string
	AcceptId    string
	MessageType string
	SendTime    time.Time
	Data        string
}
